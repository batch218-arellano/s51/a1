import {useState} from 'react';

import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';


export default function AppNavbar() {

  // State to store user information upon user login.
  const [user, setUser] = useState(localStorage.getItem('email'));


  return (
    <Navbar bg="light" expand="lg">

        
        <Navbar.Brand as = {Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">

          {/*
        "as" - prop that allows components to be treated as if they are a different component gaining access to its properties and functionalities.
        "to" - prop is used in place of the "href" prop for providing URL for the page.
        */}


          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

          {(user) ?
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
          }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}

